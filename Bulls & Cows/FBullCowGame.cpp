#include "FBullCowGame.hpp"
#include <string>
#pragma once

#include <map>
#define TMap std::map

using FString = std::string;
using int32 = int;

FBullCowGame::FBullCowGame() { Reset(); };

int FBullCowGame::GetCurrentTry() const { return MyCurrentTry; };
int32 FBullCowGame::GetHiddenWordLength() const { return MyHiddenWord.length(); }
bool FBullCowGame::IsGameWon() const { return bGameIsWon; };

int FBullCowGame::GetMaxTries() const { 
	TMap<int32, int32> WordLengthToMaxTries {
		{3,4}, 
		{4,7},
		{5,1},
		{6,15},
		{7,20},
	};
	return WordLengthToMaxTries[MyHiddenWord.length()]; 
};

void FBullCowGame::Reset() {

    const FString HIDDEN_WORD = "donkey";
    MyHiddenWord = HIDDEN_WORD;
	
    MyCurrentTry = 1;
    bGameIsWon = false;

    return;
};

EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess) const {
    if (!IsIsogram(Guess)) { // If the guess isn't an isogram, //
        return EGuessStatus::Not_Isogram;
    } else if (!IsLowercase(Guess)) { // If the guess isn't all lowercase //
        return EGuessStatus::Not_Lowercase;
    } else if (Guess.length() != GetHiddenWordLength()) { // If the guess length is wrong // 
        return EGuessStatus::Wrong_Length; 
    } else {
        return EGuessStatus::OK;
    } 
};


// Receives a Valid Guess, increments turn and returns count. //
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess) {

    MyCurrentTry++;
    FBullCowCount BullCowCount;
    int32 WordLength = MyHiddenWord.length(); // assuming the same length as guess

    // Compare letters against the hidden word //
    for (int32 MHWChar = 0; MHWChar < WordLength; MHWChar++)
    {
        // Loop through all letters in the guess. //
        for (int32 GChar = 0; GChar < WordLength; GChar++)
        {
            // If they match then //
            if (Guess[GChar] == MyHiddenWord[MHWChar])
            {
                // Check Index with MHWChar And GChar //
                if (MHWChar == GChar) {
                    BullCowCount.Bulls++;
                } else {
                    BullCowCount.Cows++;
                }
            }
        }
    }

    if (BullCowCount.Bulls == WordLength) {
        bGameIsWon = true;
    } else  {
        bGameIsWon = false;
    }

    return BullCowCount;
};

bool FBullCowGame::IsIsogram(FString Word) const {
    // Treat 0 and 1 letter strings as isograms //
    if (Word.length() <= 1) {
        return true;
    }
	
	// Setup the Map //
    TMap<char, bool> LetterSeen;

	// For all letters in word Var //
	for (auto Letter : Word) {
		Letter = tolower(Letter); // conver to lowercase to handle mixed case //
		if (LetterSeen[Letter]) {
			// If letter is in the map //
			return false; // this is not an isogram //
		} else {
			LetterSeen[Letter] = true; // Add Letter to the map as seen //
		}
    }

    return true;
}

bool FBullCowGame::IsLowercase(FString Word) const {
	for (auto Letter : Word) {
		if (!islower(Letter)) {
			return false;
		}
		else {
			return true;
		}
	}
	return false;
}
/*
    This is the console executable, that makes use of the BullCow Class
    This acts as the view win a MVC pattern, and is responsible for all user interactrion. 
    For game logic see the FBullCowGame class.
*/
#pragma once

#include <iostream>
#include "FBullCowGame.hpp"

// To Make syntax Unreal Friendly //
using FText = std::string;
using int32 = int;

// Function prototypes as outside a class //
void PrintIntro();
void PlayGame();
FText GetValidGuess();
bool AskToPlayAgain();
void PrintGameSummary(); 

FBullCowGame BCGAME; // Instantiate a New Game, which is re-used across plays//

// Entry Point for Application //
int main() {
    bool bPlayAgain = false;
    do {
        PrintIntro();
        PlayGame();
        bPlayAgain = AskToPlayAgain();
    } while (bPlayAgain);
    
    return 0; // Exit the application
}

// Introuce the Game //
void PrintIntro() {
    std::cout << "\n\nWelcome to Bulls and Cows, a fun word game" << std::endl;
    std::cout << "Can you guess the " << BCGAME.GetHiddenWordLength() << " letter isogram I'm thinking of? \n" << std::endl;
    return;
}

// Play the Game - Plays a single game to completion //
void PlayGame() {

    BCGAME.Reset();
    int32 MaxTries = BCGAME.GetMaxTries(); // Retrieve Try Count from FBullCowGame Class //

    // loop asking for guesses while the game is NOT WON and there are still tries remaining.
    while (!BCGAME.IsGameWon() && BCGAME.GetCurrentTry() <= MaxTries) {
        FText Guess = GetValidGuess();

        // Submit Valid Guess to the game and receive counts//
        FBullCowCount BullCowCount = BCGAME.SubmitValidGuess(Guess);

        std::cout << "Bulls = " << BullCowCount.Bulls;
        std::cout << ". Cows = " << BullCowCount.Cows << "\n\n";
    }


    PrintGameSummary();
    return;
}

// loop continually until user gives valid guess //
FText GetValidGuess() {

    EGuessStatus Status = EGuessStatus::Invalid_Status;
    FText Guess = "";
    do {
        // Get a guess from player
        int32 CurrentTry = BCGAME.GetCurrentTry();
        std::cout << "Try " << CurrentTry << " of " << BCGAME.GetMaxTries() << ". Enter your guess: ";
        getline(std::cin, Guess);

        Status = BCGAME.CheckGuessValidity(Guess);
        switch (Status)
        {
            case EGuessStatus::Wrong_Length:
                std::cout << "Please enter a " << BCGAME.GetHiddenWordLength() << " Letter word.\n\n";
                break;
            case EGuessStatus::Not_Isogram:
                std::cout << "Pleas enter a word without repeating letters.\n\n";
                break;
            case EGuessStatus::Not_Lowercase:
                std::cout << "Please enter all lower case letters.\n\n";
                break;
            default: 
                // Assume the guess is valid
                break;
        }
    } while(Status != EGuessStatus::OK); // Keep looping until we get no errors
    return Guess;
}

// Ask player if they'd like to play again //
bool AskToPlayAgain() {
    std::cout << "Do you want to play again with the same hidden word?";
    FText Response = "";
    getline(std::cin, Response);

    // Call character like an array //
    // Response[0]
    bool TrueValue = (Response[0] == 'y' || Response[0] == 'Y');
    std::cout << "Is it y? " << TrueValue << std::endl;
    
    return TrueValue;
}

void PrintGameSummary() {
    if (BCGAME.IsGameWon()) {
        std::cout << "WELL DONE - YOU WIN!\n";
    } else {
        std::cout << "Better luck next time \n";
    }
}
